variable "dns_name"     {}
variable "managed_zone" {}
variable "record_type"  {}
variable "record"       {}

variable "ttl" {
  default = 300
}
