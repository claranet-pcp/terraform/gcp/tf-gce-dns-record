resource "google_dns_record_set" "record" {
  name         = "${var.dns_name}"
  managed_zone = "${var.managed_zone}"
  type         = "${var.record_type}"
  ttl          = "${var.ttl}"
  rrdatas      = ["${var.record}"]
}
