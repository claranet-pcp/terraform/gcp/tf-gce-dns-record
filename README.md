# tf-gce-dns-record
## Variables
* `dns_name` -
* `managed_zone` -
* `record_type` -
* `record` -
* `ttl` (300) -

## Outputs
There are no outputs from this module

## Usage examples

```
module "sso-db-dns" {
  source = "git::ssh://git@gogs.bashton.net/Bashton/tf-gce-dns-record.git"

  dns_name     = "sso-db.${var.envname}.${module.dns_zone.dns_name}"
  managed_zone = "${module.dns_zone.name}"
  record_type  = "A" 
  record       = "${module.ss-sql.ip_address}"
}
```
